variable "repo_username" {
    type = string
    sensitive = true
}

variable "repo_password" {
    type = string
    sensitive = true
}

packer {
  required_plugins {
    amazon = {
      version = ">= 0.0.2"
      source  = "github.com/hashicorp/amazon"
    }
  }
}

source "amazon-ebs" "ubuntu" {
  ami_name      = "csgo-ubuntu-aws-${formatdate("YY.MM.DD", timestamp())}"
  instance_type = "t3.xlarge" # On AWS network performance depends on instance type, for t2.medium it will up to 50Mb but for t3.medium up to 5Gb
  region        = "ap-southeast-1"
  source_ami_filter {
    filters = {
      name                = "ubuntu/images/*ubuntu-focal-20.04-amd64-server-*"
      virtualization-type = "hvm"
    }
    most_recent = true
    owners      = ["099720109477"]
  }
  launch_block_device_mappings {
    device_name           = "/dev/sda1"
    volume_size           = 40
    volume_type           = "gp2"
    delete_on_termination = true
  }
  ssh_username = "ubuntu"
  tags = {
    based-on = "Ubuntu"
    managed-by = "Packer"
    release = "focal"
    Name = "{{ .SourceAMIName }}"
  }
}

build {
  name = "csgo-server-build"
  sources = [
    "source.amazon-ebs.ubuntu"
  ]

  provisioner "ansible" {
    playbook_file    = "./ansible/csgo-server.yml"
    user = "ubuntu"
    ansible_env_vars = ["ANSIBLE_CONFIG=./ansible/ansible.cfg"]
    extra_arguments = [
      "-e", "repo_username=${var.repo_username}",
      "-e", "repo_password=${var.repo_password}",
    ]
  }

}
