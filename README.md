# CS:GO AWS AMI #

This repository contains packer receipt and ansible role for creating AWS AMI with CS:GO game server.

### Requirements ###

To create AWS AMI will be needed:

1. Linux! Ansible can be used only with linux.
2. [Packer by Hashicorp](https://www.packer.io/)
3. [Ansible](https://www.ansible.com/)

### How to build ###

There are two options how to build the AMI.

- **"Slow"** - image will built from the scratch, as a base image it will use `Ubuntu Linux 20.04`
- **"Fast"** - as a build image will be used the latest `cs:go AMI`, it means that the previous version should be present in the region where the image will be built. For instance if it is a clear environment, that doesn't have any images, first of all use **slow** option and then **fast** can be used.

To build **slow** image run:
```bash
$ packer init
$ packer build -var repo_username=<USERNAME> -var repo_password=<PASSWORD> aws-ubuntu.csgo.pkr.hcl
```

To build **fast** image run:
```bash
$ packer init
$ packer build -var repo_username=<USERNAME> -var repo_password=<PASSWORD> aws-csgo-csgo.pkr.hcl
```

### Packer configuration variables ###

Packer requires two additional variables to build image

Those vars are needed to be able to download and use [csgo-instance-express-api](https://bitbucket.org/dynastyesports/csgo-instance-express-api/src/main/) on build time.

| NAME | DESCRIPTION|
| --- | --- |
| repo_username | bitbucket username |
| repo_password | bitbucket password |

### Ansible configuration variables ###

Ansible role could be also fine tuned if needed

There are the list of the ansible variables:

| NAME | DESCRIPTION | DEFAULT VALUE |
| ---- | ----------- | ------------- |
| csgo_server_username | Linux username that will be created to run cs:go server | steam |
| csgo_server_steamcmd_url | The URL to get steamcmd, steamcmd is a cli utility that is needed to download cs:go server | https://steamcdn-a.akamaihd.net/client/installer/steamcmd_linux.tar.gz |
| csgo_server_metamod_url | CS:GO server plugin, metamod URL, required to run get5 plugin | https://mms.alliedmods.net/mmsdrop/1.11/mmsource-1.11.0-git1145-linux.tar.gz |
| csgo_server_sourcemod_url | CS:GO server plugin, sourcemod URL, required to run get5 plugin | https://sm.alliedmods.net/smdrop/1.11/sourcemod-1.11.0-git6800-linux.tar.gz |
| csgo_server_get5_url | GET5 CS:GO server plugin, it is a matchmaking plugin | https://github.com/splewis/get5/releases/download/0.7.2/get5_0.7.2.zip |
| csgo_server_steamworks_url | CS:GO server plugin, steamworks plugin, is needed to run get5-event-api plugin | https://github.com/KyleSanderson/SteamWorks/releases/download/1.2.3c/package-lin.tgz |
| csgo_server_get5_eventapi_url | get5-event-api plugin URL | https://github.com/yannickgloster/get5_eventapi/releases/download/v1.0.0/get5_eventapi.smx |
| csgo_server_nodejs_major_version | The NodeJS version, to run [csgo-instance-express-api](https://bitbucket.org/dynastyesports/csgo-instance-express-api/src/main/) | 14 |
| csgo_server_conf_dir | dir from where server will take initial configuration | /etc/csgo |
| csgo_server_controller_conf_dir | dir from where [csgo-instance-express-api](https://bitbucket.org/dynastyesports/csgo-instance-express-api/src/main/) will take configuration | /etc/csgo |
| csgo_server_controller_repository | uri to [csgo-instance-express-api](https://bitbucket.org/dynastyesports/csgo-instance-express-api/src/main/) repo | bitbucket.org/dynastyesports/csgo-instance-express-api.git |
| csgo_server_controller_version| branch or tag name for [csgo-instance-express-api](https://bitbucket.org/dynastyesports/csgo-instance-express-api/src/main/) | main |
